from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}

    return render(request, "todo_lists/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {"todo_list": todo_list}

    return render(request, "todo_lists/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoListForm
    context = {"form": form}
    return render(request, "todo_lists/create.html", context)


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "items/todo.html", context)


def update_todo_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():

            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)

    context = {"form": form}

    return render(request, "todo_lists/edit.html", context)


def update_item_list(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        "form": form,
    }

    return render(request, "items/item.html", context)


def delete_todo_list(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")

    return render(request, "todo_lists/delete.html")
