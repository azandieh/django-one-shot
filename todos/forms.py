from django.forms import ModelForm
from todos.models import TodoItem, TodoList


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = "__all__"


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "is_completed",
            "due_date",
            "list",
        ]


# or fields = "__all__"
